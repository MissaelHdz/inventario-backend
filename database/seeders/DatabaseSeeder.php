<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\Star;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Category::factory(10)->create();
        Product::factory(200)->create();
        Star::factory(5000)->create();
    }
}
