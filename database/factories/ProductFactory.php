<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'category_id' => $this->faker->randomElement(Category::query()->pluck('id')->toArray()),
            'sku' => $this->faker->randomNumber(8),
            'image' => $this->faker->imageUrl(),
            'name' => $this->faker->word(),
            'description' => $this->faker->paragraph(),
            'price' => $this->faker->randomFloat(3 , 5 , 8),
            'stock' => $this->faker->randomNumber(2),
            'status' => $this->faker->randomElement(['Con inventario', 'Sin inventario']),
        ];
    }
}
