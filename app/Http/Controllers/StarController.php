<?php

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\Star;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class StarController extends Controller
{
    /**
     * @param Request $request
     * @param $product_id
     * @return JsonResponse
     */
    public function rate(Request $request , $product_id): JsonResponse
    {
        $product = Product::query()->find($product_id);
        if (!$product)
            return $this->errorResponse("El producto no existe");

        Star::query()->create([
            'product_id' => $product_id,
            'score' => $request->get('score')
        ]);

        return $this->successResponse();
    }
}
