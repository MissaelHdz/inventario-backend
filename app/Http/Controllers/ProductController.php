<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Star;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $products = Product::query()->orderBy('id' , 'desc')->paginate(15);

        return $this->successResponse(['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create(): JsonResponse
    {
        $categories = Category::query()->pluck('name' , 'id' )->toArray();
        $aux = [];
        foreach($categories as $key => $category)
            $aux[] = ['id' => $key, 'name' => $category,];

        return $this->successResponse(['categories' => $aux]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), Product::$rules);

        if ($validator->fails())
            return $this->errorResponse("Error en validación" , ['errors' => $validator->errors()->toArray()]);

        $data = $request->all();
        $data['status'] = ($data['stock'] > 0) ? 'Con inventario' : 'Sin inventario';
        $product = Product::query()->create($data);

        if (!$product)
            return $this->errorResponse("Ocurrió un error al registrar el producto");

        return $this->successResponse();
    }

    public function update(Request $request , $id): JsonResponse
    {
        $product = Product::query()->find($id);
        if (!$product)
            return $this->errorResponse("El producto no existe");
        $validator = Validator::make($request->all(), Product::$rules);

        if ($validator->fails())
            return $this->errorResponse("Error en validación" , ['errors' => $validator->errors()->toArray()]);

        $data = $request->all();
        $data['status'] = ($data['stock'] > 0) ? 'Con inventario' : 'Sin inventario';
        $product = $product->update($data);

        if (!$product)
            return $this->errorResponse("Ocurrió un error al actualizar el producto");

        return $this->successResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $product = Product::query()->find($id);
        if (!$product)
            return $this->errorResponse("El producto no existe");

        return $this->successResponse(['product' => $product]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $product = Product::query()->find($id);
        if (!$product)
            return $this->errorResponse("El producto no existe");

        $product->delete();

        return $this->successResponse();
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function inventory(Request $request , $id): JsonResponse
    {
        $product = Product::query()->find($id);
        if (!$product)
            return $this->errorResponse("El producto no existe");

        $product->update(['status' => $request->get('status')]);

        return $this->successResponse();
    }
}
