<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait ApiResponse
{
    /**
     * @param array $data
     * @param int $status
     * @return JsonResponse
     */
    protected function successResponse($data = [], $status = JsonResponse::HTTP_OK): JsonResponse
    {
        $body = ['success' => true, 'message' => "Acción realizada con éxito"] + $data;
        return response()->json($body, $status, [],JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param string $message
     * @param array $data
     * @param int $status
     * @return JsonResponse
     */
    protected function errorResponse($message = null , $data = [], $status = JsonResponse::HTTP_BAD_REQUEST): JsonResponse
    {
        $body = ['success' => false, 'message' => $message] + $data;
        return response()->json($body, $status, [], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }
}
