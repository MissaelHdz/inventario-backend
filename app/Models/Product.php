<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, softDeletes;

    protected $guarded = ['id'];

    protected $appends = [
      'stars' , 'category_name'
    ];

    protected $hidden = [
      'category' , 'created_at' , 'updated_at' , 'deleted_at'
    ];

    static $rules = [
        'sku' => 'required',
        'name' => 'required',
        'description' => 'required',
        'price' => 'required',
        'stock' => 'required',
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }


    public function getStarsAttribute()
    {
        return round(Star::query()->where('product_id' , $this->id)->average('score'), 1,PHP_ROUND_HALF_UP);

    }

    public function getCategoryNameAttribute(): string
    {
        if ($this->category)
            return $this->category->name;

        return "NA";
    }
}
